import pickle
import os

class Config:

    def __init__(self, filename):
        self._dico = {}
        self._fileName = "Config/"+filename

    def save(self):
        with open(self._fileName, "wb") as file:
            pickleConfig = pickle.Pickler(file)
            pickleConfig.dump(self._dico)

    def get_value(self, key):
        return self._dico[key]
            
    def set_value(self, key, value):
        self._dico[key] = value
        return value
        
    
    def load(self):
        """return TRUE si fichier exist sinon FALSE"""
        if os.path.exists(self._fileName):
            with open(self._fileName, "rb") as file:
                pickleConfig = pickle.Unpickler(file)
                self._dico = pickleConfig.load()
            return True
        else:
            self.save()
            return False
                
            
            
                
