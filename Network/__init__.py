# -*- coding: utf-8 -*-

import socket
import select
import threading
from Action import *
from Entity import *
import time

port = 25565

class Network:
    
    
    def __init__(self, isHost, ip, game):
        self.server = None
        self.client = None
        if isHost == True:
            self.server = Server(game)
            self.client = Client("localhost", game)
        else:
            self.client = Client(ip, game)

    def stop(self):
        if self.server != None:
            self.server.connexion.close()
        if self.client != None:   
            self.client.connexionServer.close()
            self.client.connected = False
        
    def send_message(self, msg):
        self.client.send_message(msg)
        
              
class GameServer():
    def __init__(self, server):
        self.turn = None
        self.online = True
        self.server = server
        
    def nextTurn(self, li):
        if self.turn == None:
            self.turn = 0
        self.turn = self.turn + 1
        if self.turn >= len(li):
            self.turn = 0
        return self.changeTurn(li, self.turn)
            
        
    def changeTurn(self, li, _id):
        _id = str(_id)
        player = li[_id]
        player.canPlay = True
        turn = YourTurn(0)
        turn = turn.get_str_function()
        for key, item in li.items():
            if str(key) != _id:
                item.canPlay = False
                self.server.sendTo(item, "server:"+turn)
        turn = YourTurn(1)
        turn = turn.get_str_function()
        self.server.sendTo(player, "server:"+turn)
        return li[_id]


class Server(threading.Thread):
    
    def __init__(self, game):
        self.hote = ''
        self.connexion = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.connexion.bind((self.hote, port))
        self.connexion.listen(5)
        self.clients = []
        self.players = {}
        #self.clientsOnline = {}
        print("server init")
        self.msg = ""
        self.game = game
        self.gameServer = GameServer(self)
        self.newMsg = False
        threading.Thread.__init__(self)
        self.start()
        
    def receivRequest(self, request, connexion):
        request = str(request)
        request = request.split(":")
        if len(request) >= 3:
            nameOrId = str(request[0])
            action = str(request[1])
            args = str(request[2]).split(",")
            if action == "Login":
                idPlayer = len(self.players)
                player = Player(str(nameOrId), "Black", idPlayer)
                player.connexion = connexion
                self.players[str(idPlayer)] = player
                login = Login()
                login.add_id(idPlayer)
                request = player.get_username()+":"+login.get_str_function()
                self.sendAllExcept(player, request) # envoie aux autre joueur déjà présent qu'un nouveau est là
                
                for ply in self.players.values(): # envoie au joueur qui vient de se co les joueurs présents
                    login = Login()
                    login.add_id(ply.id)
                    playerOnline = ply.get_username()+":"+login.get_str_function()
                    self.sendTo(player, playerOnline)
                    time.sleep(1)
                if len(self.players) == 2:
                    color = "green"
                    for ply in self.players.values():
                        selectColor = SelectColor(color)
                        self.sendAll(str(ply.id)+":"+selectColor.get_str_function())
                        ply.color = color
                        color = "red"
                        time.sleep(1)
                    time.sleep(1)
                    game = GameReady()
                    self.sendAll("server:"+game.get_str_function())
                    self.gameServer.nextTurn(self.players)
                    #game lancé !
            elif action == "ChangeColor":
                player = self.players[nameOrId]
                player.color = args[0]
                self.sendAllExcept(player, request)
            elif action == "Play":
                case = str(args[0])
                if case.count("+") >= 1:
                    case = case.replace("+", "")
                if self.game.window.grid.caseBusy(case) == True:
                    erreur = Erreur("Une pièce est déjà posée ici !")
                    player = self.players[nameOrId]
                    self.sendTo(player, "server:"+erreur.get_str_function())
                else:
                    request = self.rebuildRequest(request)
                    player = self.players[nameOrId]
                    if player.canPlay == False or self.gameServer.online == False:
                        return
                    self.sendAll(request)
                    time.sleep(1)
                    self.gameServer.nextTurn(self.players)
                    if self.game.window.grid.diagonalAligned(case, nameOrId) == True or self.game.window.grid.horizontalAligned(case, nameOrId) == True or self.game.window.grid.verticalAligned(case, nameOrId) == True:
                        player = self.players[nameOrId]
                        gg = GameFinished(1)
                        self.sendTo(player, "server:"+gg.get_str_function())
                        loose = GameFinished(0)
                        self.sendAllExcept(player, "server:"+loose.get_str_function())
                        self.gameServer.online = False
            elif action == "KeyZlatan":
                player = self.players[nameOrId]
                zlatan = "zlatan"
                arg = str(args[0]).replace("+", "")
                strg = player.key+str(arg)
                print(strg)
                if zlatan.count(strg) == 1:
                    player.key = strg
                    if len(zlatan) == len(strg):
                        keyZ = KeyZlatan(1)
                        self.sendTo(player, nameOrId+":"+keyZ.get_str_function())
                        keyZ = KeyZlatan(0)
                        self.sendAllExcept(player, nameOrId+":"+keyZ.get_str_function())
                        print("v")
                else:
                    player.key = ""
            elif action == "Message":
                request = self.rebuildRequest(request)
                self.sendAll(request)
                
    
                
    def rebuildRequest(self, request):
        newRequest = ""
        for item in request:
            newRequest = newRequest+str(item)+":"
        newRequest = str(newRequest)
        if newRequest.count("+") >= 1:
            newRequest.replace("+", "")
        return newRequest
                
                
    def sendAll(self, msg):
        msg = str(msg)
        for ply in self.players.values():
            try:
                ply.connexion.send(msg.encode())
            except:
                pass
                
    def sendTo(self, player, msg):
        msg = str(msg)
        player.connexion.send(msg.encode())
                
    def sendAllExcept(self, player, msg):
        msg = str(msg)
        for ply in self.players.values():
            if ply.get_username() != player.get_username():    
                try:
                    ply.connexion.send(msg.encode())
                except:
                    pass
        
    def run(self):
        run = True
        while run == True:
            connexions_demandees, wlist, xlist = select.select([self.connexion],
            [], [], 0.05)
     
            for connexion in connexions_demandees:
                connexion_avec_client, infos_connexion = connexion.accept()
                self.clients.append(connexion_avec_client)
            clients_a_lire = []
            try:
                clients_a_lire, wlist, xlist = select.select(self.clients,
                [], [], 0.05)
            except select.error:
                pass
            else:
                for client in clients_a_lire:
                    try:
                        msg_recu = client.recv(1024)
                        self.receivRequest(msg_recu.decode(), client)
                    except:
                        pass
                """if self.msg != "":
                    for client in self.clients:
                        try:
                            client.send(self.msg.encode())
                        except:
                            pass
                    self.msg = """""
                    
                
            
class Client(threading.Thread):
    def __init__(self, ip, game):
        self.connected = False
        self.connexionServer = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            self.connexionServer.connect((ip, port))
        except:
            return
        self.msgRecv = ""
        self.msgSend = ""
        self.game = game
        self.connected = True
        threading.Thread.__init__(self)
        self.start()
            
    def run(self):
        run = True
        self.game.sendRequestNetwork(Login())
        while run == True:
            self.connexionServer.settimeout(1)
            try:
                msg = self.connexionServer.recv(1024)
                msg = msg.decode()
                self.game.receiveRequestNetwork(msg)
            except:
                self.msgRecv = None
                pass
            if self.msgSend != "":
                msg = str(self.msgSend)
                self.connexionServer.send(msg.encode())
                self.msgSend = ""
        
            
    def send_message(self, msg):
        self.msgSend = msg
    
    
    
