import json

class Action:
    
    def __init__(self, functionName):
        self.username = functionName
        self.args = []
        
    def get_str_function(self):
        request = str(self.username)
        request = request+":"
        i = 0
        for item in self.args:
            if i > 0 and i < len(self.args):
                request = request+","
            request = request+str(item)
            i += 1
        request = request+"+"
        return request
    
    def get_name(self):
        return self.username

class KeyZlatan(Action):
    def __init__(Action, key):
        Action.__init__(self, "KeyZlatan")
        self.args.append(key)
    
class GameReady(Action):
    def __init__(self):
        Action.__init__(self, "GameReady")
        
class SelectColor(Action):
    def __init__(self, color):
        Action.__init__(self, "SelectColor")
        self.args.append(color)
        
class YourTurn(Action):
    def __init__(self, _bool):
        Action.__init__(self, "YourTurn")
        self.args.append(_bool)

class Login(Action):
    def __init__(self):
        Action.__init__(self, "Login")
        
    def add_id(self, _id):
        self.args.append(_id)
        
class Erreur(Action):
    def __init__(self, msgErreur):
        Action.__init__(self, "Erreur")
        self.args.append(msgErreur)
        
class GameFinished(Action):
    def __init__(self, gg):
        Action.__init__(self, "GameFinished")
        self.args.append(gg)
            
class Message(Action):
    def __init__(self, msg):
        Action.__init__(self, "Message")
        self.args.append(msg)
        
class Play(Action):
    
    def __init__(self, case):
        Action.__init__(self, "Play")
        self.args.append(case)
