class Player :
    
    def __init__(self, username, color, _id) :
        self.username = username
        self.color = color
        self.id = _id
        self.canPlay = False
        self.connexion = None
        self.key = ""
        
    def set_id(self, _id):
        self.id = _id
        
    def get_id(self):
        return self.id
        
        
    def get_username(self):
        return self.username
    
    def get_color(self):
        return self.color
    
    def set_username(self, username):
        self.username = username
        
