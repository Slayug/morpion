from Window import *
from Config import *
from tkinter import *
from Network import *
from Entity import *
import re

class Game():
    
    #initialisation du jeu par le constructeur de Game
    def __init__(self) :
        self.player = Player("Player", "black", -1)
        self.cfg = Config("game")
        if self.cfg.load() == True:
            self.player.set_username(self.cfg.get_value("username"))
        self.network = None
        self.players = {}
        """windowTh = WindowThread(self)
        self.window = windowTh.get_window()"""
        self.window = Window(self)
        self.window.fen.mainloop()
        
        
        
    #fonction qui doit être appelée pour sauvegarder le pseudo dans le fichier de config
    def delete(self):
        self.cfg.set_value("username", self.player.get_username())
        self.cfg.save()


    def get_username(self):
        return self.username

    def set_username(self, name):
        self.player.set_username(str(name))
        self.delete()

    """"
    fonction permettant de vérifier si l'arguement ip est bien de type ip
    X.X.X.X
    avec jusqu'à 3 X ( de 1 à 3 )possible par case, exemple
    X.XXX.X.X
    """
    def is_good_ip(self, ip):
        self.__dict__
        ip = str(ip)
        ip += "."
        expressionType ="([0-9]{1,3}[.]){4}"
        if re.search(expressionType, ip):
            return True
        return False
    
    
    #Permet d'envoyer une action (packet) par le réseau
    def sendRequestNetwork(self, action):
        request = ""
        if action.get_name() == "Login":
            request = self.player.get_username()
        else:
            request = self.player.get_id()
        request = str(request)+":"+action.get_str_function()
        print(repr(self.network))
        self.network.client.send_message(request)
        
    #fonction appelée quand le réseau reçoi une action (packet)
    #Pour ensuite traduire et faire en fonction des arguements
    def receiveRequestNetwork(self, request):
        request = str(request)
        if request.count("+") >= 1:
            requestPlus = request.split("+")
            if requestPlus[0].count("+") >= 1:
                requestPlus[0] = requestPlus[0].replace("+" "")
            if requestPlus[0].count("+") >= 1:
                requestPlus[1] = requestPlus[1].replace("+", "")
            self.receiveRequestNetwork(str(requestPlus[0]))
            request = str(requestPlus[1])
        request = request.split(":")
        if len(request) >= 3:
            nameOrId = str(request[0])
            action = str(request[1])
            args = request[2].split(",")
            if action == "Login":
                player = Player(str(nameOrId), "green", str(args[0]))
                self.players[args[0]] = player
                if str(nameOrId) == self.player.get_username():
                    self.player.id = args[0]
            elif action == "Play":
                player = self.players[nameOrId]
                self.window.grid.draw_piece(args[0], self.window.canv, player)
            elif action == "SelectColor":
                player = self.players[nameOrId]
                player.get_username()
                player.color = str(args[0])
            elif action == "KeyZlatan":
                player = self.players[nameOrId]
                self.window.grid.drawZlatan(self.window.canv, player)
                if str(args[0]) == "1":
                    self.window.message_gagne()
                    self.window.message_erreur("Vous avez Zlatane l'adversaire !")
                else:
                    self.window.message_perdu()
                    self.window.message_erreur("Zlataned !")
            elif action == "GameReady":
                firstPlayer = self.players["0"]
                secondPlayer = self.players["1"]
                self.window.jeu(firstPlayer.get_username(), secondPlayer.get_username())
            elif action == "GameFinished":
                self.network.stop()
                if str(args[0]) == "1":
                    print("Gagne !")
                    self.window.message_gagne()
                else:
                    self.window.message_perdu()
                    print("perdu")
            elif action == "Erreur":
                self.window.message_erreur(str(args[0]))
            elif action == "Message":
                player = self.players[nameOrId]
                self.window.setChat(player.get_username(), str(args[0]))
            elif action == "YourTurn":
                if int(args[0]) == 1:
                    self.player.canPlay = True
                    self.window.setTurn(True)
                else:
                    self.window.setTurn(False)
                    self.player.canPlay = False
            """elif action == "ChangeColor":
                player = self.players[nameOrId]
                player.color = args[0]"""
            






game = Game()
