from tkinter import *
from Action import *
import threading
from Network import *

import tkinter.messagebox as messagebox

class Window:
    def __init__(self, _game) :
        self.widgets = []
        self.fen = Tk()
        self.game =_game
        self.fen.geometry(str(1120)+"x"+str(800))
        self.fen.title("Gomoku")
        self.fen.configure(bg="pink")
        
        
        
        self.grid = GridCanvas()       
        modeQuestion = StringVar()
        labelModeQuestion = Label(self.fen, textvariable=modeQuestion, font=("Tahoma", 18), bg="pink",height=2)
        modeQuestion.set("Bienvenue sur Gomoku en réseau ")
        
        self.pack(labelModeQuestion)
        
        fieldPseudo = Label(self.fen, text="Pseudo : ", fg="black", bg="pink", font=("Tahoma", 16))
        self.pack(fieldPseudo)

        self.pseudoUser = StringVar()
        pseudo = Entry(textvariable=self.pseudoUser)
        self.pseudoUser.set(self.game.player.get_username())
        self.pack(pseudo)

        labelvide = Label(self.fen, text=" ", bg="pink", )
        self.pack(labelvide)
        
        buttonpseaudo= Button(self.fen, text= "Valider", command=self.changePseudo, bg="green", font=("Tahoma", 16))
        self.pack(buttonpseaudo)

        labelvide = Label(self.fen, text=" ", bg="pink", )
        self.pack(labelvide)
        
        buttonMulti= Button(self.fen,text="C'est parti ! ", command=self.multi,activebackground="white", bg="green", font=("Tahoma", 16))
        self.pack(buttonMulti)

        
        
        
        self.chatContent = StringVar()
        self.turnContent = StringVar()
        self.canv = Canvas(self.fen,width=760,height=760)
      
    def changePseudo(self):
        self.game.set_username(self.pseudoUser.get())
        labelPseudo= Label(self.fen, text="Votre pseudo a été enregistré ", fg="black", bg="pink", font=("Tahoma", 16))
        self.pack(labelPseudo)
        
    def pack(self, widget, _side=TOP):
        widget.pack(side=_side)
        self.widgets.append(widget)
        
    def message_erreur(self, msg):
        fenError = Tk()
        fenError.geometry(str(400)+"x"+str(150))
        fenError.configure(bg="Red")      
        msg = Label( fenError, text=msg, font=("Tahoma", 18), bg="Red",height=2)
        msg.pack()
        

        buttonnouvelle= Button(fengag, text= "Recommencer une nouvelle partie", command=self.multi, bg="cyan",font=("Tahoma", 12))
        buttonnouvelle.pack()

    def reset(self):
        for widget in self.widgets:
            widget.destroy()
        self.widgets = []
        
    def multi(self):
        self.reset()
        
        Modeadresse = StringVar()
        adresse = Label(self.fen, textvariable=Modeadresse, font=("Tahoma", 16), bg="pink", height=2)
        Modeadresse.set("Entrer une adresse IP : ")
        adresse.grid(row=0,column=0)
        self.pack(adresse)
        
        
        IP = StringVar()
        
        entry=Entry(textvariable=IP)
        self.pack(entry)
        
        labelvide = Label(self.fen, text=" ", bg="pink" )
        self.pack(labelvide)
        
        buttonConnect= Button(self.fen,text="Se Connecter",command=lambda ip=IP.get() : self.connect(IP.get()), bg="green", font=("Tahoma", 16))
        self.pack (buttonConnect)

        labelvide = Label(self.fen, text=" ", bg="pink", )
        self.pack(labelvide)

        labelOU = Label(self.fen, text=" OU ", bg="pink", font=("Tahoma", 16))
        self.pack(labelOU)
        
        labelvide = Label(self.fen, text=" ", bg="pink")
        self.pack(labelvide)
        
        buttonCreateGame = Button(self.fen,text="Créer une partie",command=self.createGame, bg="green", font=("Tahoma", 16))
        self.pack (buttonCreateGame)
        
        
    def createGame(self):
        self.reset()
        textWaitingPlayer = StringVar()
        labelWaitingPlayer = Label(self.fen, textvariable=textWaitingPlayer, font=("Tahoma", 16),bg="pink")
        textWaitingPlayer.set("En attente de joueurs")
        self.pack(labelWaitingPlayer)
        self.game.network = Network(True, None, self.game)
        self.waitingPlayer(textWaitingPlayer)
        
    def waitingPlayer(self, text):
        text.set(text.get()+".")
        textStr = str(text.get())
        if textStr.count(".") > 3:
            text.set("En attente de joueurs")
        self.fen.after(2000, self.waitingPlayer, text)
        
        
    def connect (self, ip):
        self.reset()
        if True == self.game.is_good_ip(ip):
            self.game.network = Network(False, ip, self.game)
            if self.game.network.client.connected == False:
                messagebox.showerror("Erreur", "Aucun serveur sur cette adresse dommage..")
                self.multi()
            else:
                login = Login()
                self.game.sendRequestNetwork(login)
                #self.selectColor()
        else :
            messagebox.showerror("Erreur", "Votre adresse n'est pas correct. Veuillez réessayer...")
            self.multi()
        
     
  
    def clicked(self, event):
        case = self.grid.get_case(event.x, event.y)
        play = Play(case)
        self.game.sendRequestNetwork(play)

    def key(self, event):
        key = str(event.char)
        zlatan = "zlatan"
        if zlatan.count(key) >= 1:
            keyZ = KeyZlatan(key)
            self.game.sendRequestNetwork(keyZ)

    def jeu(self, firstPLayer, secondPlayer) :
        self.reset()
        self.canv.bind("<Button-1>",self.clicked)
        self.fen.bind("<Key>", self.key)
        self.pack(self.canv, LEFT)
        self.canv.create_rectangle((2,2,self.grid.width,self.grid.height),fill="white",outline="black")
        i=0
        n=0
        while i<=18:
            self.canv.create_line((0,n,self.grid.width,n),fill="black",width=1)
            n=n+self.grid.widthCase
            i = i + 1
        i=0
        n=0
        while i<=18 :
            self.canv.create_line((n,0,n,self.grid.height),fill="black",width=1)
            i=i+1
            n=n+self.grid.heightCase
            
        versusFirstLabel = Label(self.fen, text=str(firstPLayer), fg="red", font=("Rockwell Extra Bold", 20), bg="pink")
        self.pack(versusFirstLabel, TOP)
        
        versusSecondLabel = Label(self.fen, text="VS\n"+str(secondPlayer), fg="green", font=("Rockwell Extra Bold", 20),bg="pink")
        self.pack(versusSecondLabel, TOP)
        
        chatLabel = Label(self.fen, textvariable=self.chatContent, bg="pink")
        self.chatContent.set("Chat: \n")
        self.pack(chatLabel)
        
        
        turnLabel = Label(self.fen, textvariable=self.turnContent,bg="pink")
        self.pack(turnLabel)
    
        msgEntry = StringVar()
        entry = Entry(textvariable=msgEntry)
        self.pack(entry)
        buttonMsg= Button(self.fen,text="Envoyer",command=lambda msg=msgEntry.get() : self.msgEnter(msgEntry.get()), bg="green")
        self.pack (buttonMsg)

    def setChat(self, who, msg):
        msg = str(who)+": "+str(msg)+"\n"
        self.chatContent.set(self.chatContent.get()+msg)
        
    def setTurn(self, _bool):
        if _bool == True:
            self.turnContent.set("C'est ton tour !")
        else:
            self.turnContent.set("C'est pas ton tour !")
            
        
    def msgEnter(self, msg):
        msg = Message(msg)
        self.game.sendRequestNetwork(msg)

    def message_gagne(self):
        fengag= Tk()
        fengag.configure(bg="Red")
       
        gagne = Label( fengag, text="Vous avez gagné !!! ", font=("Tahoma", 18), bg="Red",height=2)
        
        gagne.pack()
        

        buttonnouvelle= Button(fengag, text= "Recommencer une nouvelle partie", command=self.multi, bg="cyan",font=("Tahoma", 12))
        buttonnouvelle.pack()
        
    

    def message_perdu(self):
        fengag= Tk()
        fengag.configure(bg="lightGray")
   
        perdu= Label(fengag, text="Vous avez perdu...", relief=RAISED, font=("Tahoma", 16), bg="lightGray",height=2)
      
        perdu.pack()
   

        buttonnouvelle= Button(fengag, text= "Recommencer une nouvelle partie", command=self.multi, bg="cyan",font=("Tahoma", 12))
        buttonnouvelle.pack()
            
        
        
class GridCanvas:
    def __init__(self):
        self.width = 760
        self.height = 760
        self.widthCase = 40
        self.heightCase = 40
        self.align = 5
        self.cases = []
        i = 0
        while i < 361:
            self.cases.append(-1)
            i = i + 1
        

    def get_width(self):
        return self.width
    def get_height(self):
        return self.height
    
    def get_case(self, x, y):
        xCase = int(x/self.widthCase)
        yCase = int(y/self.heightCase)
        return int(yCase * (self.width/self.widthCase) + xCase)

        
    def aligned(self, case, _id, incremente):
        increment = int(incremente)
        count = 1
        case = int(case)
        _id = int(_id)
        i = 0
        case = int(case)
        caseCheck = case
        while i < self.align or self.align == count:
            caseCheck = caseCheck - increment
            if self.cases[caseCheck] == _id:
                count = count + 1
            else:
                break
            i = i + 1
            if self.checkInArray(caseCheck) == False:
                break
        if count == self.align:
            return True
        caseCheck = case
        while i < self.align or self.align == count:
            caseCheck = caseCheck + increment
            if self.cases[caseCheck] == _id:
                count = count + 1
            else:
                break     
            i = i + 1
            if self.checkInArray(caseCheck) == False:
                break
        if count == self.align:
            return True
        return False
    
    def verticalAligned(self, case, _id):
        return self.aligned(case, _id, self.height/self.heightCase)
    
    def horizontalAligned(self, case, _id):
        return self.aligned(case, _id, 1)
    
    def diagonalAligned(self, case, _id):
        length = int(self.height/self.heightCase)
        return self.aligned(case, _id, length-1) or self.aligned(case, _id, length+1)
        
    
    def checkInArray(self, case):
        if case < 0 or case > (self.width/self.widthCase)*(self.height/self.heightCase)-1:
            return False
        return True
    
    def caseBusy(self, case):
        case = int(case)
        if self.cases[case] == -1:
            return False
        return True

    def drawZlatan(self, canvas, player):
        case = 0
        while case < len(self.cases):
            y = int(case / (self.height/self.heightCase))*self.heightCase
            x = (case - (y/self.heightCase)*(self.width/self.widthCase))*self.widthCase
            self.cases[case] = int(player.id)
            canvas.create_oval((x+5, y+5, x+32, y+32), fill=player.get_color(),outline=player.get_color())
            case = case + 1
    
    def draw_piece(self, case, canvas, player):
        case = int(case)
        y = int(case / (self.height/self.heightCase))*self.heightCase
        x = (case - (y/self.heightCase)*(self.width/self.widthCase))*self.widthCase
        self.cases[case] = int(player.id)
        canvas.create_oval((x+5, y+5, x+32, y+32), fill=player.get_color(),outline=player.get_color())
        return case

class WindowThread(threading.Thread):
    def __init__(self, game):
        threading.Thread.__init__(self)
        self.game = game
        self.window = Window(self.game)
        self.start()
        
    def get_window(self):
        return self.window
 
 
    def run(self):
        self.window.fen.mainloop()
   

class SelectColors:
    def __init__(self, window, player, colors):
        self.colors = {}
        var = StringVar()
        self.window = window
        for color in colors:
            radioButton = Radiobutton(window.fen, text=color, variable=var, value=color, command=lambda content=var.get() : self.changeColor(var.get()))
            if color == player.color:
                radioButton.select()
            self.colors[color] = radioButton
        usernameContent = StringVar()
        self.usernameLabel = Label(window.fen, textvariable=usernameContent)
        usernameContent.set(player.username)
        self.username = player.get_username()
        
    def changeColor(self, content):
        print(content)
        change = self.changeColor(content)
        self.window.game.sendRequestNetwork(change)
        
    def pack(self):
        self.usernameLabel.pack()
        for color in self.colors.values():
            color.pack()
            
    def destroy(self):
        self.usernameLabel.destroy()
        for color in self.colors.values():
            color.destroy()
