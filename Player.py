from Config import *

class Player :
    
    def __init__(self) :
        self.username = "Player"


    def __getstate__(self):
        dicoState = {}
        dicoState["USERNAME"] = self.username
        return dicoState

    def __setstate__(self, dict_attr):
        self.username = dict_attr["USERNAME"]
